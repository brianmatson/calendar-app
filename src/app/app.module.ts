import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CalendarPageComponent } from './components/calendar-page/calendar-page.component';
import { WeekComponent } from './components/week/week.component';
import { DayComponent } from './components/day/day.component';
import { GetMonthService } from './services/get-month.service';
import { WeekHeaderComponent } from './components/week-header/week-header/week-header.component'; 
import {DialogModule} from 'primeng/dialog';
import {MatDialogModule} from '@angular/material/dialog';
import { AppointmentDialogComponent } from './components/appointment-dialog/appointment-dialog.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent,
    CalendarPageComponent,
    WeekComponent,
    DayComponent,
    WeekHeaderComponent,
    AppointmentDialogComponent
  ],
  imports: [
    BrowserAnimationsModule,
    DialogModule,
    BrowserModule,
    MatDialogModule
  ],
  providers: [GetMonthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
