import { Component, OnInit } from '@angular/core';
import { GetMonthService } from '../../services/get-month.service';
import { Day } from '../../domain/objects'; 
import * as moment from 'moment';
import {DialogModule} from 'primeng/dialog';


@Component({
  selector: 'app-calendar-page',
  templateUrl: './calendar-page.component.html',
  styleUrls: ['./calendar-page.component.css']
})
export class CalendarPageComponent implements OnInit {
  private days:Day[];
  private weeks:Day[][];
  private monthName:string;
  constructor(private getMonthService:GetMonthService) { }
  
  ngOnInit() {  
    this.days = this.getMonthService.getDates();
    let curWeek:Day[]=[];
    this.weeks=[];
    this.weeks.push(curWeek);
    //fill empty slots for week one;
    for (let i=0;i<this.days[0].dayOfWeek;i++)
      curWeek.push(<Day>{date:-1,dayOfWeek:-1});

    for (let i=0;i<this.days.length;i++) {
      curWeek.push(<Day>{date:this.days[i].date,dayOfWeek:this.days[i].dayOfWeek});
      if (this.days[i].dayOfWeek === 6) {
        curWeek=[];
        this.weeks.push(curWeek);
      }
    }
   let empty=this.weeks[this.weeks.length-1].length;
    for (let i=6;i>=empty;i--)
      curWeek.push(<Day>{date:-1,dayOfWeek:-1});

    this.monthName=moment().format('MMMM');
  }


  
}
