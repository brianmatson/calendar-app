import { Component, OnInit, Input, Inject} from '@angular/core';
import { Day } from '../../domain/objects';
import { AppointmentDialogComponent } from '../appointment-dialog/appointment-dialog.component';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-day',
  templateUrl: './day.component.html',
  styleUrls: ['./day.component.css']
})
export class DayComponent implements OnInit {
  @Input() private day:Day;

  constructor(public dialog: MatDialog) { }

  ngOnInit() {
  }

  openDialog(): void {
    let dialogRef = this.dialog.open(AppointmentDialogComponent, {
      width: '250px',
      data: { day: this.day },
      
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
     
    });
  }
}

