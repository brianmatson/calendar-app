import { Component, OnInit, Input } from '@angular/core';
import { Day } from '../../domain/objects';


@Component({
  selector: 'app-week',
  templateUrl: './week.component.html',
  styleUrls: ['./week.component.css'] 
})
export class WeekComponent implements OnInit {
  @Input() private days:Day[];
  constructor() { }

  ngOnInit() {
  }

}
