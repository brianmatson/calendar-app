import { TestBed, inject } from '@angular/core/testing';

import { GetMonthService } from './get-month.service';

describe('GetMonthService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GetMonthService]
    });
  });

  it('should be created', inject([GetMonthService], (service: GetMonthService) => {
    expect(service).toBeTruthy();
  }));
});
