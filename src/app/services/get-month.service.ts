import { Injectable } from '@angular/core';
import { Day, Month} from '../domain/objects';
import * as moment from 'moment';

@Injectable()
export class GetMonthService {

  constructor() { }

  public getDates(): Day[] {
    const today = moment();
    const month = moment(today).month();
    const first = moment().date(1);
   
    let weekday =  moment(first).weekday();
    const maxDate= moment(first).add(1, 'month').subtract(1,'day').date();

    var days =[];
    for (let i=1;i<=maxDate;i++) {
       let day:Day = <Day>{
        date:i,
        dayOfWeek:weekday
      }
      days.push(day);
      weekday = (weekday +1)%7;
    }
    return days;
  } 
}
